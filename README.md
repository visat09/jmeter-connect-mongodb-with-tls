# Jmeter - Connect MongoDB with TLS



## Prerequisite

1) MongoDB 4.0.0
2) Java version 1.8.0_191-b12
3) Apache-Jmeter-5.1+ (With Mongo-java-driver-3.12.10.jar, groovy-3.0.10.jar)

## Medium

- [How to generate test data in MongoDB using Jmeter with TLS, How to use Jmeter with TLS to create test data in MongoDB](https://visarutsaepueng.medium.com/how-to-generate-test-data-in-mongodb-using-jmeter-with-tls-how-to-use-jmeter-with-tls-to-create-1c76f3599166)